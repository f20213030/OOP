import java.lang.Math;

public class Records {
    int key;
    double value;
    public Records (double x) {
        this.key = (int) Math.ceil(x);
        this.value = x;
    }

    public int getKey() {
        return key;
    }

    public double getValue() {
        return value;
    }
}
